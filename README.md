# first_method_deduplication_problem

**Capable of running on both Rstudio and version of R 3.4.3 or upgraded version**

***IN R-STUDIO*** For file reading goto Session->Set Working Directory->Choose Directory...->**Then browse to the folder where sample excel file is present for ex. G:\study\Innovaccer hackercamp**->Click Open  

**Pre-requisite installation of packages NTP, tm, SnowballC on R compiler**

*Input file must be named "Deduplication Problem - Sample Dataset.csv"*

*The model will create an output file named "first_method_result.csv" on the same folder as input excel file*